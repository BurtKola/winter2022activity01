import java.util.Scanner;

public class PartThree{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Enter side length of square");
		int squareLength = reader.nextInt();
		
		System.out.println("Enter side length of rectangle");
		int rectangleLength = reader.nextInt();
		System.out.println("Enter side width of rectangle");
		int rectangleWidth = reader.nextInt();
		
		int squareArea = AreaComputations.areaSquare(squareLength);
		AreaComputations ac = new AreaComputations();
		int rectangleArea = (ac.areaRectangle(rectangleLength, rectangleWidth));
		
		System.out.println("The area of the square is " +squareArea+ " and the area of the rectangle is " +rectangleArea+ ".");
	}
}